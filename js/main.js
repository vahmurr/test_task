
var lastCityCode = null;
var lastDegrees  = null;
var lastDays     = null; // I like this var name. 8) Repent of your sins!
var lastQuery    = 0;
var chart        = null;
var weatherData  = null;

/*
 * First load
 */
$(document).ready(function() {


    /*
     * Google map init in modal
     */
    $('#myMapModal').on('shown.bs.modal', function() {

        var countrySelect = $('#countries');
        var country       = countrySelect.val();
        var countryName   = countrySelect.find(':selected').text();
        var citySelect    = $('#cities');
        var cityName      = citySelect.find(':selected').text();

        $.getJSON('json/'+country+'.json', function (data) {

            var cityCode = citySelect.val();

            var lon = null;
            var lat = null;

            data.forEach(function(elem){
                if(elem._id == cityCode){
                    lon = elem.coord.lon;
                    lat = elem.coord.lat;
                }
            });

            if(lon && lat) {
                mapInit(lon, lat);
            }

            $("#map-modal-title").text(countryName + ' - ' + cityName);

        }).fail( function() {
            printError('File json/' + country + '.json is missing. Put it back!');
            $("#map-modal-title").empty();

        });

    });

    var spinner = $("#spinner");

    /*
     * Degrees switcher init
     */
    $('#degrees').bootstrapSwitch({
        state: false,
        size: 'small',
        checked: true,
        onText: 'Farenheit',
        offText: 'Celsius',
        handleWidth: 72
    });

    /*
     * Empty datatable
     */
    fillDatatable({});

    /*
     * Days spinner init
     */
    spinner.TouchSpin({
        min: 5,
        max: 16
    });

    /*
     * Decline spinner keyboard input
     */
    spinner.bind("keydown", function (event) {
        event.preventDefault();
    });

    /*
     * Countries Select2 init
     */
    $.getJSON('json/countries.json', function (data) {

        $("#countries").select2({
            width: '100%',
            tags: false,
            placeholder: "Select country..",
            data:  data
        });
    }).fail( function() {
        printError('File json/countries.json is missing. Put it back!');
    });

    /*
     * Cities Select2 init
     */
    $("#cities").select2({
        width: '100%',
        minimumResultsForSearch: -1,
        placeholder: "Select country first",
        disabled: "disabled"
    });

    /*
     * Button Get Weather click
     */
    $('#btn-get-weather').click(function() {

        if( !$(this).hasClass('disabled') ) {
            var switcherState   = $('#degrees').bootstrapSwitch('state');
            var degrees         = '';
            var days            = $('#spinner').val();
            var cityCode        = $('#cities').val();
            var currentTime     =  new Date().getTime();


            /*
             * Inputs validation
             */
            if (days < 5 || days > 16) {
                alert('Number of days should be between 5 and 16');
                return false;
            }
            if (!cityCode) {
                alert('You must select a city');
                return false;
            }

            /*
             * Converting switcher values [true|false] to degrees system
             */
            if (switcherState) {
                degrees = 'imperial';
            }
            else {
                degrees = 'metric';
            }

            /*
             * We cannot spam API calls on free version of openweathermap-api
             * Minute timeout if form data is same
             */
            if( !(degrees == lastDegrees && cityCode == lastCityCode && days == lastDays && currentTime-lastQuery <= 60000) ) {

                lastDegrees  = degrees;
                lastCityCode = cityCode;
                lastDays     = days;
                lastQuery    = currentTime;
                updateWeather(cityCode, days, degrees);
            }

        }
    });
});


/*
 * Show and fill modal weather
 */
$(document.body).on('click', '.full-weather-link', function() {
    var sliderDate = $(this).attr('data-timestamp');
    fillModalWeather(sliderDate, window.weatherData);

    /*
     * If we pressing buttons next-prew, modal already opened
     */
    if(!($(this).hasClass('weather-arrow'))) {
        $('#weather-modal').modal('show');
    }
});


/*
 * After select2 "countries" change behavior
 */
$(document.body).on("change","#countries",function() {

    var country = this.value;
    /*
     * Clear select2
     */
    $('#cities').html('').select2({data: [{id: '', text: ''}],width: '100%'});

    /*
     * Cities Select2 fill
     */
    $.getJSON('json/'+country+'.json', function (data) {

        var compiledData = $.map(data, function (obj) {
            var cities = {};
            cities.id = obj._id;
            cities.text = obj.name;
            return cities;
        });

        $("#cities").select2({
            width: '100%',
            data: compiledData,
            allowClear: true,
            placeholder: "Select city..",
            disabled: false
        });

        /*
         * Disabling button(city still not selected)
         */
        $('#btn-get-weather').addClass('disabled');
        $('#question').addClass('disabled');
    }).fail( function() {
        printError('File json/' + country + '.json is missing. Put it back!');
        $('#btn-get-weather').addClass('disabled');
        $('#question').addClass('disabled');
    });

});


/*
 * After select2 "cities" change behavior
 */
$(document.body).on("change","#cities",function() {

    var getWeather = $('#btn-get-weather');

    if( !$(this).val() ) {
        getWeather.addClass('disabled');
        $('#question').addClass('disabled');
    }
    else {
        getWeather.removeClass('disabled');
        $('#question').removeClass('disabled');
    }

});


/*
 * Update weather by openweathermap API-call
 * API-key Default - 2cba4fe5431ae618bf0b82453034ee9b
 * In real life this call must be executed on the server side
 */
function updateWeather(cityCode, days, degrees) {

    $.getJSON('http://api.openweathermap.org/data/2.5/forecast/daily?id='+cityCode+'&mode=json&units='+degrees+'&cnt='+days+'&APPID=2cba4fe5431ae618bf0b82453034ee9b', function(data) {
            clearDatatable();
            clearCharts();
            window.weatherData = data;
            fillDatatable(data);
            fillCharts(data);
        })
        .fail(function() {
                printError('No OpenWeatherMap API responce. You can...hit your system administrator.');
                clearDatatable();
                clearCharts();
            }
        );
}


/*
 * Error output
 */
function printError(errorText) {

    var forAlerts = $('#for-alerts');
    forAlerts.empty();
    forAlerts.html(
        '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Error!</strong> ' + errorText + '</div>'
    );

}


/*
 * Weather datatables init function
 */
function fillDatatable(data) {

    var degreesSign = ($('#degrees').bootstrapSwitch('state') === true) ? 'F' : 'C';

    $('#weather-table').dataTable({
        "aaData"        : data.list,
        "bFilter"       : false,
        "bInfo"         : false,
        "bLengthChange" : false,
        "pagingType"    : "simple",
        "bDestroy"      : true,
        "pageLength"    : 5,
        "aoColumns": [
            {
                "mDataProp" : "dt",
                "render"    : function (data) {
                    var date = new Date(data*1000);
                    return '<a data-timestamp="' + data + '" class="full-weather-link" title="show more info">' + getCompiledDate(date) + '</a>';
                },
                "sTitle"    : "Date"
            },
            {
                "mDataProp" : "weather[0].icon",
                "sTitle"    : "Weather",
                "bSortable" : false,
                "render"    : function (data, type, full) {
                    /*
                     * Weather image...label gets data from another jason column
                     */
                    return '<img src="http://openweathermap.org/img/w/' + data + '.png" class="weather-icon" title="' + full.weather[0].description + '"/>';
                }
            },
            {
                "mDataProp" : "temp.day",
                "sTitle"    : "Day &deg" + degreesSign,
                "render"    : function (data) {
                    return (data > 0) ? '+' + data : data;
                }
            },
            {
                "mDataProp" : "temp.night",
                "sTitle"    : "Night &deg" + degreesSign,
                "render"    : function (data) {
                    return (data > 0) ? '+' + data : data;
                }
            }

        ],
        "oLanguage": {
            "sEmptyTable" : "Please select country and city first..."
        }
    });

    /*
     * Setting weather table headline
     */
    if ( Object.keys(data).length !== 0 ) {
        var numberOfDays = $('#spinner').val() - 1;
        var myDate       = new Date();
        var timestamp    = myDate.getTime();
        var today        = getCompiledDate(timestamp);
        var last         = getCompiledDate(timestamp + numberOfDays*24*60*60*1000);
        $("#dt-header").text( today + " - " + last + " : " + $("#countries").find('option:selected').text() + " - " + $("#cities").find('option:selected').text() );
    }

}


/*
 * Weather charts graph init function
 */
function fillCharts(data) {

    var linesData =
    {
        labels: [],
        datasets:
            [
                {
                    label: "Day",
                    data: [],
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: "rgba(255, 52, 38, 0.4)",
                    borderColor: "rgba(255, 52, 38, 1)",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: "rgba(255, 52, 38, 1)",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: "rgba(255, 52, 38, 1)",
                    pointHoverBorderColor: "rgba(220,220,220,1)",
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10
                },
                {
                    label: "Night",
                    data: [],
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: "rgba(75,192,192,0.4)",
                    borderColor: "rgba(75,192,192,1)",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: "rgba(75,192,192,1)",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: "rgba(75,192,192,1)",
                    pointHoverBorderColor: "rgba(220,220,220,1)",
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10
                }
            ]

    };

    /*
     * Preparing graph data(lines and labels)
     */
    data.list.forEach(function(entry) {

        /*
         * Lines
         */
        linesData.datasets[0].data.push({x: entry.dt, y: entry.temp.day});
        linesData.datasets[1].data.push({x: entry.dt, y: entry.temp.night});

        /*
         * Labels
         */
        var labelTimestamp = new Date(entry.dt*1000);
        var locale = "en-us";
        var month = labelTimestamp.toLocaleString(locale, { month: "short" });
        var labelDate = labelTimestamp.getDate() + ' ' + month;
        linesData.labels.push(labelDate);

    });

    /*
     * Drawing chartlines
     */
    var ctx = $("#myChart");
    window.chart = new Chart(ctx, {

        type: 'line',
        data: linesData,
        options: {
            responsive: false,
            scales: {
                yAxes: [
                    {
                        ticks: {
                            /*
                             * Add +(plus) sign to yaxis
                             */
                            callback: function(label) {
                                return (label > 0)? '+' + label : label;
                            }

                       }
                    }
                ]
            },
            tooltips: {
                enabled: true,
                mode: 'single',
                callbacks: {
                    /*
                     * Add +(plus) sign to tooltips
                     */
                    label: function(tooltipItems, data) {
                        var degrees =  ( tooltipItems.yLabel > 0 )? '+' + tooltipItems.yLabel : tooltipItems.yLabel;
                        return data.datasets[tooltipItems.datasetIndex].label + ' : ' + degrees; // Mother of God...
                    }
                }
            }
        }
    });

}


/*
 * Clearing datatable
 */
function clearDatatable() {

    $('#weather-table').DataTable().clear();
    window.weatherData = null;
    fillDatatable({});
    $("#dt-header").empty();

}


/*
 * Clearing chart
 */
function clearCharts() {

    if(window.chart !== null) {
        window.chart.destroy();
    }
    $("#myChart").empty();

}


/*
 * Date formatting
 */
function getCompiledDate(timestamp) {

    var date = new Date(timestamp);
    var month = date.getMonth() + 1;
    var day = date.getDate();
    return ((String(day).length > 1 ? day : "0" + day)) + '-' + (String(month).length > 1 ? month : "0" + month)  + '-' + date.getFullYear();

}


/*
 * Map showing in modal
 */
function mapInit(lng, lat) {

    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: lat, lng: lng},
        zoom: 12
    });
    var currentCenter = map.getCenter();
    google.maps.event.trigger(map, "resize");
    map.setCenter(currentCenter);

}


/*
 * Fill modal weather
 */
function fillModalWeather(timestamp, data) {

    for (var i = 0, len = data.list.length; i < len; i++) {

        if(data.list[i].dt == timestamp) {

            /*
             * next-prev buttons
             */
            var prevLink  = $("#weather-modal-prev");
            var nextLink  = $("#weather-modal-next");
            var prewArrow = $("#weather-modal-prev-arrow");
            var nextArrow = $("#weather-modal-next-arrow");

            var switcherState = $('#degrees').bootstrapSwitch('state');
            var degrees = (switcherState)? ' &degF' : ' &degC';

            /*
             * Changing title
             */
            var countryName   = $('#countries').find(':selected').text();
            var cityName      = $('#cities').find(':selected').text();
            $("#weather-modal-title").text(countryName + ' - ' + cityName);

            /*
             * Changing table
             */
            $('.modal-weather-data').empty();
            $('#modal-weather-picture').html('<img src="http://openweathermap.org/img/w/' + data.list[i].weather[0].icon + '.png" class="weather-icon" title="' + data.list[i].weather[0].description + '"/>');
            $('#modal-weather-date').html(getCompiledDate(timestamp*1000));
            $('#modal-weather-morning').html(( data.list[i].temp.morn > 0)? '+' + data.list[i].temp.morn + degrees : data.list[i].temp.morn + degrees);
            $('#modal-weather-day').html(( data.list[i].temp.day > 0)? '+' + data.list[i].temp.day + degrees : data.list[i].temp.day + degrees);
            $('#modal-weather-evening').html(( data.list[i].temp.eve > 0)? '+' + data.list[i].temp.eve + degrees : data.list[i].temp.eve + degrees);
            $('#modal-weather-night').html(( data.list[i].temp.night > 0)? '+' + data.list[i].temp.night + degrees : data.list[i].temp.night + degrees);

            if(i != 0) {
                prevLink.attr('data-timestamp', data.list[i-1].dt);
                prewArrow.removeClass('disabled');
            }
            else {
                prevLink.removeAttr('data-timestamp');
                prewArrow.addClass('disabled');
            }

            if(i != len-1) {
                nextLink.attr('data-timestamp', data.list[i+1].dt);
                nextArrow.removeClass('disabled');
            }
            else {
                nextLink.removeAttr('data-timestamp');
                nextArrow.addClass('disabled');
            }
        }
    }
}

